import styled from "styled-components";

export const Container = styled.div`
  text-align: center;
`;

export const List = styled.div`
  min-height: 100vh;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  font-size: calc(10px + 2vmin);
  color: white;
`;
export const FavoritesList = styled.div`
  background-color: #a2b9bc;
  display: flex;
  flex-wrap: wrap;
`;
