import { DigimonProvider } from "./Digimons";
import { ReactNode } from "react";

interface ProviderProps {
  children: ReactNode;
}

const Providers = ({ children }: ProviderProps) => {
  return <DigimonProvider>{children}</DigimonProvider>;
};
export default Providers;
