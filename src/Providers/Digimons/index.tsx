import {
  createContext,
  useContext,
  useEffect,
  useState,
  ReactNode,
} from "react";
import axios from "axios";
import { Digimon } from "../../types/digimon";

interface DigimonProviderProps {
  children: ReactNode;
}

interface DigimonProviderData {
  digimons: Digimon[];
  favorites: Digimon[];
  addDigimon: (digimon: Digimon) => void;
  deleteDigimon: (digimon: Digimon) => void;
}

export const DigimonContext = createContext<DigimonProviderData>(
  {} as DigimonProviderData
);

export const DigimonProvider = ({ children }: DigimonProviderProps) => {
  const [digimons, setDigimons] = useState<Digimon[]>([] as Digimon[]);
  const [favorites, setFavorites] = useState<Digimon[]>([] as Digimon[]);

  useEffect(() => {
    axios
      .get<Digimon[]>("https://digimon-api.vercel.app/api/digimon")
      .then((res) => setDigimons(res.data));
  }, []);

  const addDigimon = (digimon: Digimon) => {
    !favorites.includes(digimon) && setFavorites([...favorites, digimon]);
  };

  const deleteDigimon = (digimonToBeDeleted: Digimon) => {
    const newList = favorites.filter(
      (digimon) => digimon.name !== digimonToBeDeleted.name
    );
    setFavorites(newList);
  };

  return (
    <DigimonContext.Provider
      value={{ digimons, favorites, addDigimon, deleteDigimon }}
    >
      {children}
    </DigimonContext.Provider>
  );
};

export const useDigimons = () => useContext(DigimonContext);
