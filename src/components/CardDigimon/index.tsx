import { Container, Image } from "./styles";
import Button from "../Button";
import { Digimon } from "../../types/digimon";
import { useDigimons } from "../../Providers/Digimons";

interface DigimonCardPros {
  isFavorites?: boolean;
  digimon: Digimon;
}

const DigimonCard = ({ isFavorites, digimon }: DigimonCardPros) => {
  const { name, level, img } = digimon;
  const { addDigimon, deleteDigimon } = useDigimons();
  return (
    <Container>
      <div>{name}</div>
      <Image src={img}></Image>
      <div>{level}</div>
      {isFavorites ? (
        <Button deleted={true} onClick={() => deleteDigimon(digimon)}>
          Remove
        </Button>
      ) : (
        <Button onClick={() => addDigimon(digimon)}>Add</Button>
      )}
    </Container>
  );
};

export default DigimonCard;
