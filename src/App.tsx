import React from "react";
import "./App.css";
import { useDigimons } from "./Providers/Digimons";
import { Container, List, FavoritesList } from "./styles";
import Digimons from "./components/Digimons";

function App() {
  const { digimons, favorites } = useDigimons();
  return (
    <div className="App">
      <header className="App-header">
        <Container>
          <FavoritesList>
            <Digimons isFavorite={true} digimons={favorites} />
          </FavoritesList>
          <List>
            <Digimons digimons={digimons} />
          </List>
        </Container>
      </header>
    </div>
  );
}

export default App;
